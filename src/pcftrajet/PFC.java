package pcftrajet;

import java.util.Random;
import java.util.Scanner;

public class PFC {

    public static void main(String[] args) {
      
        Scanner saisie = new Scanner(System.in);
        Random rd = new Random();
        
        int ordi = rd.nextInt(3)+1, player;
        String playerTxt = null, ordiTxt = null, resultat = null;
        
        System.out.println("Pierre (1), Feuille(2) ou Ciseaux(3) ?");
        player = saisie.nextInt();
        
        if(ordi == 1) {
            ordiTxt = "Pierre";
        }
        else if(ordi == 2) {
            ordiTxt = "Feuile";
        }
        else {
            ordiTxt = "Ciseaux";
        }
        if(player == 1) {
            playerTxt = "Pierre";
            if(ordi == 1) {
                resultat = "vous faites égalité";
            }
            else if(ordi == 2) {
                resultat = "vous avez perdu";
            }
            else {
                resultat = "vous avez gagné";
            }
        }
        if(player == 2) {
            playerTxt = "Feuille";
            if(ordi == 1) {
                resultat = "vous avez gagné";
            }
            else if(ordi == 2) {
                resultat = "vous faites égalité";
            }
            else {
                resultat = "vous avez perdu";
            }
        }
        if(player == 3) {
            playerTxt = "Ciseaux";
            if(ordi == 1) {
                resultat = "vous avez perdu";
            }
            else if(ordi == 2) {
                resultat = "vous avez gagné";
            }
            else {
                resultat = "vous faites égalité";
            }
        }
        
        System.out.println("Vous avez choisi: " + playerTxt + " et l'ordinateur a choisi: " + ordiTxt + " donc " + resultat + ".");
        
    }
}

        

