
package pcftrajet;

import java.util.Scanner;

public class Trajet {
    
    public static void main(String[] args) {
        
        Scanner saisie = new Scanner(System.in);
        
        int heure = 0, minute = 0, time = 0;
        float km = 0, vitesse = 0;
        
        System.out.println("Veuillez saisir le kilomètrage:");
        km = saisie.nextFloat();
        
        System.out.println("Veuillez saisir la vitesse moyenne:");
        vitesse = saisie.nextFloat();
        
        System.out.println("Veuillez saisir l'heure de départ (heure):");
        heure = saisie.nextInt();
        
        System.out.println("Veuillez saisir l'heure de départ (minute):");
        minute = saisie.nextInt();
        
        time = (int) (heure + (km / vitesse));
        System.out.println("Vous arriverez à " + (int) time + "h" + minute);
        
    }
}
